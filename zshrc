########################
#  ______       _ _    #
# |__  (_)_ __ (_) |_  #
#   / /| | '_ \| | __| #
#  / /_| | | | | | |_  #
# /____|_|_| |_|_|\__| #
#                      #
########################

source ~/.local/share/zinit/zinit.git/zinit.zsh
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

zinit load zsh-users/zsh-history-substring-search
zinit load ytet5uy4/fzf-widgets
zinit load softmoth/zsh-vim-mode 

zinit wait lucid for                                  \
		b4b4r07/enhancd                                   \
		zdharma/fast-syntax-highlighting                  \
		zsh-users/zsh-completions                         \
		mdumitru/fancy-ctrl-z                             \
		zshzoo/magic-enter                                \
	atload:_zsh_autosuggest_start                       \
		zsh-users/zsh-autosuggestions

################################################################
#  ____  _             _          ____             __ _        #
# |  _ \| |_   _  __ _(_)_ __    / ___|___  _ __  / _(_) __ _  #
# | |_) | | | | |/ _` | | '_ \  | |   / _ \| '_ \| |_| |/ _` | #
# |  __/| | |_| | (_| | | | | | | |__| (_) | | | |  _| | (_| | #
# |_|   |_|\__,_|\__, |_|_| |_|  \____\___/|_| |_|_| |_|\__, | #
#                |___/                                  |___/  #
################################################################

# enhancd
export ENHANCD_ENABLE_HYPHEN=false
export ENHANCD_ENABLE_DOUBLE_DOT=false
export ENHANCD_COMPLETION_BEHAVIOR="list"
export ENHANCD_FILTER="fzf"

# eza
export EZA_ICON_SPACING=2

# fzf
export FZF_DEFAULT_OPTS="--reverse --border --height 20 --prompt='  ' --color=dark,bg+:-1"
bindkey '^r' fzf-insert-history

# zsh-history-substring-search
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
export HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND='fg=default,bold'
export HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND='fg=red,bold'

#######################################
#   ____                           _  #
#  / ___| ___ _ __   ___ _ __ __ _| | #
# | |  _ / _ \ '_ \ / _ \ '__/ _` | | #
# | |_| |  __/ | | |  __/ | | (_| | | #
#  \____|\___|_| |_|\___|_|  \__,_|_| #
#                                     #
#######################################

# Non-program specific variable setup
export GOPATH="$HOME/.go"
export PATH="$PATH:$HOME/.local/bin:$HOME/.cargo/bin:$HOME/.go/bin:$HOME/.scripts/"
export EDITOR="nvim"
export PAGER="nvimpager"
export MANPAGER="nvimpager"

# Keep history a-la oh-my-zsh
setopt INC_APPEND_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
export HISTFILE=~/.zsh_history
export HISTSIZE=8192
export SAVEHIST=16384

# Make hitting escape more responsive with vimode
export KEYTIMEOUT=1

#####################################
#     _    _ _                      #
#    / \  | (_) __ _ ___  ___  ___  #
#   / _ \ | | |/ _` / __|/ _ \/ __| #
#  / ___ \| | | (_| \__ \  __/\__ \ #
# /_/   \_\_|_|\__,_|___/\___||___/ #
#                                   #
#####################################

# Cargo
alias cb='cargo build'
alias cbr='cargo build --release'
alias cr='cargo run'
alias crr='cargo run --release'
alias cs='cargo search'

# Git
alias ga='git add'
alias gap='git add -p'
alias gc='git commit -v'
alias gccp='git cherry-pick'
alias gcp='git commit -vp'
alias gca='git commit -va'
alias gch='git checkout'
alias gchm='git checkout master'
alias gd='git diff'
alias gst='git status'
alias gp='git push'

# Grep
alias grep='grep --color'
alias sgrep='grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS} '
alias egrep='grep -E'
alias fgrep='grep -F'

# ls
alias ls='eza --time-style long-iso --group-directories-first'
alias lsa="ls -a --icons --git"
alias l='ls -l --icons --git'
alias la='ls -la --icons --git'

# Edit ranger so we cwd on quit
alias ranger='. ranger'

# Rust
alias rb='rustc'

# Replace vim with nvim (muscle memory is hard to fix)
alias vim='nvim'
alias vimdiff='nvim -d'
alias svim='sudoedit'

# Pacman
alias pacaur='PACMAN="powerpill" pacaur'
alias pacls='pacman -Qi | grep "Name\|Description" | cut -d \: -f 2 | awk " {print;} NR % 2 == 0 { print "\n"; }"'
alias paclsexplicit='pacman -Qe | pacman -Qi | grep "Name\|Description" | cut -d \: -f 2 | awk " {print;} NR % 2 == 0 { print "\n"; }" | less'
alias reflect='$HOME/.scripts/reflect'

# Other
alias spt='systemctl --user start spotifyd.service && spt && systemctl --user stop spotifyd.service'
alias ytd='yt-dlp'
alias ytm='yt-dlp -x --audio-format mp3'

# Magic Enter options
function magic-enter-cmd {
	echo && echo
	echo "eza -l --icons --git --time-style long-iso --group-directories-first"
}

function pacrmorphans(){
	if [[ -n $(pacman -Qdt) ]]
	then
		sudo pacman -Rs $(pacman -Qtdq) 2>/dev/null
	else
		echo No orphaned files found!
	fi
}

function paclsorphans(){
	if [[ -n $(pacman -Qdt) ]]
	then
		for i in $(pacman -Qdt | cut -d " " -f 1)
		do
			pacman -Qi $i | grep "Name\|Description" | cut -d \: -f 2 | awk " {print;} NR % 2 == 0 { print "\n"; }"
		done
	else
		echo No orphaned files found!
	fi
}

# Autoadded by cpan
PATH="$HOME/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"$HOME/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"; export PERL_MM_OPT;

# Set up starship theme
RPS1=
eval "$(starship init zsh)"


