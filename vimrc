"""""""""""""""""""""""""
"  ____  _              "
" |  _ \| |_   _  __ _  "
" | |_) | | | | |/ _` | "
" |  __/| | |_| | (_| | "
" |_|   |_|\__,_|\__, | "
"                |___/  "
"""""""""""""""""""""""""

set nocompatible              " be iMproved, required
set hidden
set encoding=utf-8
filetype off                  " required

" Set Signature to use dynamic highlighting before initialization
" This lets marks be colored by gitgutter status per-line
let g:SignatureMarkTextHLDynamic = 1

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
                                                        " set the runtime path to include Plug and initialize
call plug#begin('~/.vim/plugins')
Plug 'dense-analysis/ale'                                         " Syntax checker / Lint, replacing Syntastic
Plug 'Raimondi/delimitMate'                             " Automated closing of parentheses/quotations
Plug 'junegunn/fzf'                                     " fzf binary prepackaged for vim
Plug 'junegunn/fzf.vim'                                 " Series of file/buffer selectors using fzf
Plug 'junegunn/goyo.vim', { 'on': 'Goyo' }              " Disables all UI for better focus
Plug 'yggdroot/indentline'                              " Shows levels of indention
Plug 'ggandor/leap.nvim'
Plug 'junegunn/limelight.vim', { 'on': 'Limelight' }    " Dims all not-focused paragraphs
Plug 'scrooloose/nerdtree',  { 'on': 'NERDTreeToggle' } " File browser
Plug 'majutsushi/tagbar', { 'on': 'TagbarToggle' }      " Shows tags, ordered by scope, in a pane. (UNCONFIGURED)
Plug 'NLKNguyen/papercolor-theme'                       " Bright theme
Plug 'joshdick/onedark.vim'                             " Dark theme
Plug 'rust-lang/rust.vim'                               " Provides rust syntax highlighting
Plug 'godlygeek/tabular', { 'on': 'Tabularize' }        " Simple but extensible alignment
Plug 'wellle/targets.vim'                               " Adds various text objects
Plug 'mbbill/undotree', { 'on': 'UndotreeToggle' }      " Undo visualizer, replacement for Gundo
Plug 'SirVer/ultisnips'                                 " Enhanced snippets
Plug 'vim-airline/vim-airline'                          " Replacement for powerline
Plug 'vim-airline/vim-airline-themes'                   " Themes for airline
Plug 'ryanoasis/vim-devicons'                           " Adds NerdFont support to various plugins
Plug 'tpope/vim-commentary'                             " Quick commenting
Plug 'fadein/vim-FIGlet', { 'on': 'FIGlet' }            " Quick ascii art
Plug 'tpope/vim-fugitive'                               " Tools for working in git repos
Plug 'airblade/vim-gitgutter'                           " Show added/removed lines since last git commit
Plug 'jeffkreeftmeijer/vim-numbertoggle'                " Switches between hybrid and absolute line numbers
Plug 'sheerun/vim-polyglot'                             " Also syntax coloring for many different filetypes
Plug 'kshenoy/vim-signature'                            " Display marks left of line numbers
Plug 'honza/vim-snippets'                               " Pre configured snippets for many languages
Plug 'tpope/vim-surround'                               " Quick commands to surround words/lines
Plug 'christoomey/vim-tmux-navigator'                   " Vim-aware tmux movement
Plug 'tpope/vim-unimpaired'                             " Adds a lot of bindings based off of brackets
Plug 'mg979/vim-visual-multi', {'branch': 'master'}     " Multiple cursors
Plug 'vimwiki/vimwiki'                                  " Personal wiki, mostly for notes.
Plug 'shougo/vinarise', { 'on': 'Vinarise' }            " Make vim a hex editor
Plug 'Valloric/YouCompleteMe'                           " Tab completion

call plug#end()            " required
filetype plugin indent on  " required

"""""""""""""""""""""""""""""""""""""""
"   ____                           _  "
"  / ___| ___ _ __   ___ _ __ __ _| | "
" | |  _ / _ \ '_ \ / _ \ '__/ _` | | "
" | |_| |  __/ | | |  __/ | | (_| | | "
"  \____|\___|_| |_|\___|_|  \__,_|_| "
"                                     "
"""""""""""""""""""""""""""""""""""""""

" Sets how many lines of history VIM has to remember
set history=700

" Enable filetype plugins
filetype plugin on
filetype indent on

" Set shift-tab to un-indent
nnoremap <S-Tab> <<
imap <S-Tab> <C-d>

" Set to auto read when a file is changed from the outside
set autoread

" With a map leader it's possible to do extra key combinations
map , <Leader>

" Line numbers, please
set number

" Syntax highlighting
syntax on

" Redraw a lil' faster
set ttyfast

" Enable mouse in all modes
set mouse=a
if !has('nvim')
  set ttymouse=xterm2
endif

""""""""""""""""""""""""""""""""""""""""""""""""""
" _   _ ___   _____                    _         "
"| | | |_ _| |_   _|_      _____  __ _| | _____  "
"| | | || |    | | \ \ /\ / / _ \/ _` | |/ / __| "
"| |_| || |    | |  \ V  V /  __/ (_| |   <\__ \ "
" \___/|___|   |_|   \_/\_/ \___|\__,_|_|\_\___/ "
"                                                "
""""""""""""""""""""""""""""""""""""""""""""""""""

" Better color, please.
" set t_Co=256
" let g:onedark_termcolors=256
set termguicolors
colorscheme onedark
"
" Pretty theme setup.
set laststatus=2
set noshowmode

" Gvim tweaks
set guifont=mplus\ nerd\ font\ 11
set guioptions=aig

""""""""""""""""""""""""""""""""""""""""""""""""""""
"  _   ___  __  _____                    _         "
" | | | \ \/ / |_   _|_      _____  __ _| | _____  "
" | | | |\  /    | | \ \ /\ / / _ \/ _` | |/ / __| "
" | |_| |/  \    | |  \ V  V /  __/ (_| |   <\__ \ "
"  \___//_/\_\   |_|   \_/\_/ \___|\__,_|_|\_\___/ "
"                                                  "
""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Turn on the WiLd menu
set wildmenu

" Ignore compiled files
set wildignore=*.o,*~,*.pyc

" A buffer becomes hidden when it is abandoned
set hid

" Disable splash screen
set shortmess+=at

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Show matching brackets when text indicator is over them
set showmatch

" How many tenths of a second to blink when matching brackets
set mat=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Clear search highlighting
map <silent> <leader>/ :noh<cr>

" Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove

" Useful mappings for splits
map <leader>\ :vsplit<cr>
map <leader>- :split<cr>

" Allow for w3m style scrolling
map <silent> J j<C-e>
map <silent> K k<C-y>

" Yank and paste use clipboard instead of vim buffer
" This will not work with minimal vim!
map <leader>y "+y
map <leader>yy "+yy
map <leader>P "+P
map <leader>p "+p

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  ____  _             _          ___        _   _                  "
" |  _ \| |_   _  __ _(_)_ __    / _ \ _ __ | |_(_) ___  _ __  ___  "
" | |_) | | | | |/ _` | | '_ \  | | | | '_ \| __| |/ _ \| '_ \/ __| "
" |  __/| | |_| | (_| | | | | | | |_| | |_) | |_| | (_) | | | \__ \ "
" |_|   |_|\__,_|\__, |_|_| |_|  \___/| .__/ \__|_|\___/|_| |_|___/ "
"                |___/                |_|                           "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""
" Airline "
"""""""""""

" Use fancy 24b onedark theme
let g:airline_theme='onedark'

" Use powerline glyphs
let g:airline_powerline_fonts = 1

" Enable shiny tab/bufferline
let g:airline#extensions#tabline#enabled = 1

"""""""""""
" fzf.vim "
"""""""""""

map <C-f> :Files<cr>
map <C-b> :Buffers<cr>

" Set up fzf as floating window
let g:fzf_layout = { 'window': { 'width': 0.5, 'height': 0.8, 'yoffset': 0.90 } }
let g:fzf_preview_window = 'right:65%'

" Set a few options when in fzf context
if has('nvim') && !exists('g:fzf_layout')
  autocmd! FileType fzf
  autocmd  FileType fzf set laststatus=0 noshowmode noruler
    \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler
endif

""""""""""""""""""""
" Goyo / Limelight "
""""""""""""""""""""

nmap <silent> cog :Goyo<cr>
nmap <silent> cof :Limelight!! 0.7<cr>

let g:goyo_width = 120

autocmd! User GoyoEnter Limelight0.7|set linebreak|map j gj|map k gk
autocmd! User GoyoLeave Limelight!|set nolinebreak|unmap j|unmap k

""""""""
" Leap "
""""""""
lua require('leap').add_default_mappings()

""""""""""""""
"  NERDTree  "
""""""""""""""

noremap <Leader>cd :NERDTree<cr>

"""""""""""
" Tabular "
"""""""""""
noremap <leader>a :Tabularize /

"""""""""""""""""""""""""""
" Tmux Navigator Bindings "
"""""""""""""""""""""""""""

let g:tmux_navigator_no_mappings = 1

noremap <silent> <C-h> :TmuxNavigateLeft<cr>
noremap <silent> <C-j> :TmuxNavigateDown<cr>
noremap <silent> <C-k> :TmuxNavigateUp<cr>
noremap <silent> <C-l> :TmuxNavigateRight<cr>
noremap <silent> <C-\> :TmuxNavigatePrevious<cr>

" Same bindings, but for insert mode

inoremap <silent> <C-h> <C-o>:TmuxNavigateLeft<cr>
inoremap <silent> <C-j> <C-o>:TmuxNavigateDown<cr>
inoremap <silent> <C-k> <C-o>:TmuxNavigateUp<cr>
inoremap <silent> <C-l> <C-o>:TmuxNavigateRight<cr>
inoremap <silent> <C-\> <C-o>:TmuxNavigatePrevious<cr>

""""""""""
" Tagbar "
""""""""""

nmap <leader>tt :TagbarToggle<CR>

" Rust support
let g:tagbar_type_rust = {
  \ 'ctagstype' : 'rust',
  \ 'kinds' : [
    \'T:types,type definitions',
    \'f:functions,function definitions',
    \'g:enum,enumeration names',
    \'s:structure names',
    \'m:modules,module names',
    \'c:consts,static constants',
    \'t:traits',
    \'i:impls,trait implementations',
  \]
\}

"""""""""""""
" Undo Tree "
"""""""""""""

nmap <leader>u :UndotreeToggle<cr>

let g:undotree_WindowLayout = 4
let g:undotree_SplitWidth = 32

"""""""""""""
" Ultisnips "
"""""""""""""
let g:UltiSnipsExpandTrigger="<cr>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

""""""""""""
" Vim Wiki "
""""""""""""

let g:vimwiki_list = [{'path': '~/.vimwiki', 'path_html': '~/.vimwiki/html'}]

""""""""""""""""
" Visual Multi "
""""""""""""""""
let g:VM_show_warnings = 0
let g:VM_set_statusline = 0
let g:VM_silent_exit = 1

let g:VM_maps = {}

let g:VM_maps["Find Under"]                  = '<C-n>'
let g:VM_maps["Find Subword Under"]          = '<C-n>'
let g:VM_maps["Select All"]                  = '\\A'
let g:VM_maps["Start Regex Search"]          = '\\/'
let g:VM_maps["Add Cursor Down"]             = '<C-A-J>'
let g:VM_maps["Add Cursor Up"]               = '<C-A-K>'
let g:VM_maps["Add Cursor At Pos"]           = '\\\'

let g:VM_maps["Visual Regex"]                = '\\/'
let g:VM_maps["Visual All"]                  = '\\A'
let g:VM_maps["Visual Add"]                  = '\\a'
let g:VM_maps["Visual Find"]                 = '\\f'
let g:VM_maps["Visual Cursors"]              = '\\c'

""""""""""""
" Vinarise "
""""""""""""

nmap <leader><leader>v :Vinarise<cr>

"""""""""""""""""
" YouCompleteMe "
"""""""""""""""""

let g:ycm_rust_src_path = '$HOME/.build/rust/src/rustc-1.20.0-src/'
let g:ydm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ydm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
