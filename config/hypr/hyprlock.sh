#!/usr/bin/env sh

grim -o HDMI-A-1 /tmp/screenshot-HDMI-A-1.png 
grim -o DVI-D-1 /tmp/screenshot-DVI-D-1.png 
sleep 1
hyprlock 
rm /tmp/screenshot-HDMI-A-1.png /tmp/screenshot-DVI-D-1.png
