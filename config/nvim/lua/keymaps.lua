-- Set up leader
vim.keymap.set('', ',', '<leader>')

-- Use tab for quick indention
vim.keymap.set('n', '<Tab>', '>>', {
    noremap = true,
    silent = true,
    desc = 'Quick indent'
})
vim.keymap.set('n', '<S-Tab>', '<<', {
    noremap = true,
    silent = true,
    desc = 'Quick unindent'
})
vim.keymap.set('i', '<S-Tab>', '<C-d>', {
    noremap = true,
    silent = true,
    desc = 'Quick unindent'
})

-- Move selected line up/down
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Clear search highlighting
vim.keymap.set('', '<leader>/', ':noh<cr>', {
    noremap = true,
    silent = true,
    desc = 'Clear search highlighting'
})

-- Useful mappings for managing tabs
vim.keymap.set('', 'tn', ':tabnew<cr>', {
    noremap = true,
    silent = true,
    desc = 'Create new tab'
})
vim.keymap.set('', 'tc', ':tabclose<cr>', {
    noremap = true,
    silent = true,
    desc = 'Close tab'
})

-- Useful mappings for managing buffers
vim.keymap.set('', 'gb', ':bn<cr>', {
    noremap = true,
    silent = true,
    desc = 'Next buffer'
})
vim.keymap.set('', 'gB', ':bp<cr>', {
    noremap = true,
    silent = true,
    desc = 'Previous buffer'
})

-- Useful mappings for splits
vim.keymap.set('', '<leader>\\', ':vsplit<cr>', {
    noremap = true,
    silent = true,
    desc = 'Split vertically'
})
vim.keymap.set('', '<leader>-', ':split<cr>', {
    noremap = true,
    silent = true,
    desc = 'Split horizontally'
})

-- Allow for scrolling ignoring linewrap
vim.keymap.set('', 'H', 'h', {
    noremap = true,
    silent = true,
    desc = 'Move left'
})
vim.keymap.set('n', 'J', 'gj', {
    noremap = true,
    silent = true,
    desc = 'Move down (ignoring linenumber)'
})
vim.keymap.set('n', 'K', 'gk', {
    noremap = true,
    silent = true,
    desc = 'Move up (ignoring linenumber)'
})
vim.keymap.set('', 'L', 'l', {
    noremap = true,
    silent = true,
    desc = 'Move right'
})
-- Yank and paste use clipboard instead of vim buffer
-- This will not work with minimal vim!
vim.keymap.set('', '<leader>y', '"+y', { desc = "Yank to system clipboard" })
vim.keymap.set('', '<leader>P', '"+P', { desc = "Paste backwards from system clipboard" })
vim.keymap.set('', '<leader>p', '"+p', { desc = "Paste from system clipboard" })
