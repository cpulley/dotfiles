local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    -- Better handling of huge files
    'pteroctopus/faster.nvim',

    -------------------------------------------------------
    --                    _   _          _   _           --
    --     /\            | | | |        | | (_)          --
    --    /  \   ___  ___| |_| |__   ___| |_ _  ___ ___  --
    --   / /\ \ / _ \/ __| __| '_ \ / _ \ __| |/ __/ __| --
    --  / ____ \  __/\__ \ |_| | | |  __/ |_| | (__\__ \ --
    -- /_/    \_\___||___/\__|_| |_|\___|\__|_|\___|___/ --
    -------------------------------------------------------
    --                Form over function                 --
    -------------------------------------------------------

    -- Dark theme
    "joshdick/onedark.vim",
    "catppuccin/nvim",

    -- Shows levels of indention
    -- Replacing "yggdroot/indentline",
    {
        "lukas-reineke/indent-blankline.nvim",
        main = "ibl",
        dependencies = "HiPhish/rainbow-delimiters.nvim",
        config = function()
            local highlight = {
                "RainbowViolet",
                "RainbowBlue",
                "RainbowGreen",
                "RainbowYellow",
                "RainbowOrange",
            }

            local hooks = require "ibl.hooks"
            -- create the highlight groups in the highlight setup hook, so they are reset
            -- every time the colorscheme changes
            hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
                vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#D19A66" })
                vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#E5C07B" })
                vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#98C379" })
                vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#61AFEF" })
                vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#C678DD" })
            end)

            vim.g.rainbow_delimiters = { highlight = highlight }
            require("ibl").setup { scope = { highlight = highlight } }
        end
    },

    -- Show added/removed lines since last git commit
    "airblade/vim-gitgutter",

    -- Display marks left of line numbers
    "kshenoy/vim-signature",

    -- Replacement for powerline
    {
        init = function()
            vim.cmd([[
            let g:airline_theme='catppuccin'
            let g:airline_powerline_fonts=1
            let g:airline#extensions#tabline#enabled=1

            " powerline symbols
            let g:airline_left_sep = ''
            let g:airline_left_alt_sep = ''
            let g:airline_right_sep = ''
            let g:airline_right_alt_sep = ''
            ]])
        end,
        "vim-airline/vim-airline",
        dependencies = {
            "vim-airline/vim-airline-themes", -- Add themes
        },
        lazy = false,
    },

    {
        'goolord/alpha-nvim',
        dependencies = { 'nvim-telescope/telescope.nvim' },
        config = function()
            local alpha = require 'alpha'
            local dashboard = require 'alpha.themes.dashboard'
            dashboard.section.header.val = {
                [[ ░▒▓███████▓▒░░▒▓████████▓▒░▒▓██████▓▒░░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░▒▓██████████████▓▒░  ]],
                [[ ░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░     ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ]],
                [[ ░▒▓█▓▒░░▒▓█▓▒░▒▓██████▓▒░░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ]],
                [[ ░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░     ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▓█▓▒░ ░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ]],
                [[ ░▒▓█▓▒░░▒▓█▓▒░▒▓████████▓▒░▒▓██████▓▒░   ░▒▓██▓▒░  ░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ]],
            }
            dashboard.section.header.opts = {
                position = "center",
                hl = "comment"
            }

            dashboard.section.buttons.val = {
                dashboard.button("e", "󰈔 New file", ":ene <BAR> startinsert <CR>"),
                dashboard.button("u", "󱝩 Recently opened files", ":Telescope oldfiles <CR>"),
                dashboard.button("f", "󰱼 Find file", ":Telescope find_files <CR>"),
                dashboard.button("d", "󱈆 Find dotfile", ":cd ~/Documents/Repos/dotfiles <CR> :Telescope find_files <CR>"),
                dashboard.button("q", "󰅚 Quit NVIM", ":qa<CR>"),
            }

            alpha.setup(dashboard.config)

            -- Disable folding on alpha buffer
            vim.cmd([[
                autocmd FileType alpha setlocal nofoldenable
            ]])
        end
    },

    -- Give hints for multi-key commands
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        init = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 500
            require("which-key").setup()
        end,
        config = function()
            local wk = require("which-key")
            -- -- TODO: get folder names working
            wk.register({ ["<space>w"] = { name = "+Workspace" } })
            wk.register({ ["<space>rn"] = { name = "Rename symbol in all buffers" }, })
        end,
        keys = { ",", "," },
        opts = {
        }
    },

    ------------------------------------------------
    --   _    _  _    _  _  _  _    _             --
    --  | |  | || |  (_)| |(_)| |  (_)            --
    --  | |  | || |_  _ | | _ | |_  _   ___  ___  --
    --  | |  | || __|| || || || __|| | / _ \/ __| --
    --  | |__| || |_ | || || || |_ | ||  __/\__ \ --
    --   \____/  \__||_||_||_| \__||_| \___||___/ --
    ------------------------------------------------
    --           Bigger tools with TUIs           --
    ------------------------------------------------

    -- LSP manager
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
    "neovim/nvim-lspconfig",

    -- Automatically show signatures while typing
    -- Some configuration sits in lsp.lua
    {
        "ray-x/lsp_signature.nvim",
        event = "VeryLazy",
        opts = {
            bind = true,
            hint_prefix = "󱞩 ",
            handler_opts = {
                border = "none"
            },
        },
        config = function(_, opts)
            require('lsp_signature').setup(opts)
        end
    },
    -- Vscode-like pictograms
    {
        "onsails/lspkind.nvim",
        event = { "VimEnter" },
    },

    -- Enhanced snippets
    {
        "SirVer/ultisnips",
        dependencies = {
            "honza/vim-snippets", -- pre-made snippets
        },
        init = function()
            vim.g.UltiSnipsExpandTrigger = "<F12>"
            vim.g.UltiSnipsJumpForwardTrigger = "<a-n>"
            vim.g.UltiSnipsJumpBackwardTrigger = "<a-p>"
        end
    },

    -- Auto-completion engine
    {
        "hrsh7th/nvim-cmp",
        dependencies = {
            "onsails/lspkind.nvim",
            "hrsh7th/cmp-nvim-lsp", -- lsp auto-completion
            "hrsh7th/cmp-buffer",   -- buffer auto-completion
            "hrsh7th/cmp-path",     -- path auto-completion
            "hrsh7th/cmp-cmdline",  -- cmdline auto-completion
            "nvim-tree/nvim-web-devicons",
            "quangnguyen30192/cmp-nvim-ultisnips",
            "SirVer/ultisnips"
        },
        config = function() require("plugins.nvim-cmp") end
    },
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        dependencies = { 'nvim-treesitter/nvim-treesitter-textobjects' },
        config = function()
            require("plugins.nvim-treesetter")
            -- local configs = require("nvim-treesitter.configs")
        end
    },

    -- Fuzzy finding interfaces
    {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.6', -- or, branch = '0.1.x',
        dependencies = {
            'nvim-lua/plenary.nvim',
            'BurntSushi/ripgrep',
            'nvim-telescope/telescope-fzf-native.nvim',
            'SalOrak/whaler.nvim',
            'nvim-neo-tree/neo-tree.nvim'
        },
        opts = {
            defaults = {
                theme = "ivy"
            }
        },
        keys = {
            { '<C-f>', "<cmd>Telescope find_files theme=ivy<cr>", "Fuzzy find files" },
            { '<C-g>', "<cmd>Telescope live_grep theme=ivy<cr>",  "Fuzzy find using ripgrep" },
            { '<C-b>', "<cmd>Telescope buffers theme=ivy<cr>",    "Fuzzy find buffers" },
            { '<C-p>', "<cmd>Telescope commands theme=ivy<cr>",   "Fuzzy find commands" },
            { '<C-s>', function()
                local whale = require('telescope').extensions.whaler.whaler
                whale({
                    --settings for whaler here
                    directories = { '/home/cpulley/Workspace', '/home/cpulley/Documents/Repos/dotfiles' }, -- Path directories to search. By default the list is empty.
                    oneoff_directories = { '/home/user/.config/nvim', '/home/cpulley' },                   -- Path directories to append directly to list of projects. By default is empty.
                    auto_file_explorer = true,                                                             -- Whether to automatically open file explorer. By default is `true`
                    auto_cwd = true,                                                                       -- Whether to automatically change current working directory. By default is `true`
                    file_explorer = "neotree",                                                             -- Automagically creates a configuration for the file explorer of your choice.
                    -- Options are "netrw"(default), "nvimtree", "neotree", "oil", "telescope_file_browser".
                    file_explorer_config = {                                                               -- (OPTIONAL) Map to configure what command is triggered by which plugin.
                        hidden = false,                                                                    -- Show hidden directories or not (default false)
                        -- For basic configuration this is done automatically setting up the file_explorer config.
                        plugin_name = "neotree",                                                             -- Plugin. Should be installed.
                        command = "Neotree",                                                              -- The plugin command to open.
                    },
                    theme = {                                                                              -- Telescope theme default Whaler options.
                        results_title = false,                                                             -- Either `false` or a string.
                        layout_strategy = "horizontal",
                        previewer = true,
                        layout_config = {
                            height = 0.5,
                            width = 0.99,
                            anchor = "S"
                        },
                        sorting_strategy = "ascending",
                        border = true,
                    }
                })
            end, "Change working directory" }
        }
    },

    {
        "nvim-neo-tree/neo-tree.nvim",
        branch = "v3.x",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
            "MunifTanjim/nui.nvim",
            -- "3rd/image.nvim", -- Optional image support in preview window: See `# Preview Mode` for more information
        },
        keys = {
            { '<leader><tab>', '<cmd>Neotree show toggle<cr>', 'Open directory tree' }
        },
        config = function()
            require("neo-tree").setup({
                close_if_last_window = true,
                git_status = {
                    symbols = {
                        -- Change type
                        added     = "  ", -- or "✚", but this is redundant info if you use git_status_colors on the name
                        modified  = "  ", -- or "", but this is redundant info if you use git_status_colors on the name
                        deleted   = "✖ ", -- this can only be used in the git_status source
                        renamed   = "󰁕 ", -- this can only be used in the git_status source
                        -- Status type
                        untracked = " ",
                        ignored   = " ",
                        unstaged  = "󰄱 ",
                        staged    = " ",
                        conflict  = " ",
                    }
                }
            })
        end
    },

    -- File selection and manipulation using ranger
    {
        "kelly-lin/ranger.nvim",
        config = function()
            require("ranger-nvim").setup({
                replace_netrw = true,
                ui = {
                    border = "rounded",
                    height = 0.5,
                    width = 1,
                    x = 0.5,
                    y = 0,
                }
            })
            vim.api.nvim_set_keymap("n", "<leader>cd", "", {
                noremap = true,
                callback = function()
                    require("ranger-nvim").open(true)
                end
            })
        end,
    },

    {
        "jiaoshijie/undotree",
        dependencies = "nvim-lua/plenary.nvim",
        config = true,
        keys = { -- load the plugin only when using it's keybinding:
            { "<leader>u", "<cmd>lua require('undotree').toggle()<cr>", desc = "Open undotree" },
        },
    },



    ----------------------------------------------------
    --   ______                _   _                  --
    --  |  ____|              | | (_)                 --
    --  | |__ _   _ _ __   ___| |_ _  ___  _ __  ___  --
    --  |  __| | | | '_ \ / __| __| |/ _ \| '_ \/ __| --
    --  | |  | |_| | | | | (__| |_| | (_) | | | \__ \ --
    --  |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/ --
    ----------------------------------------------------
    --          Smaller tools, mapped to keys         --
    ----------------------------------------------------

    -- Simple but extensible alignment
    {
        "godlygeek/tabular",
        keys = {
            { "<leader>a", ":Tabularize /", desc = "Tabularize selection" },
        },
    },

    -- Adds various text objects
    "wellle/targets.vim",

    -- Quick commenting
    "tpope/vim-commentary",

    -- Automated closing of parentheses/quotations
    "Raimondi/delimitMate",

    -- Tools for working in git repos
    "tpope/vim-fugitive",

    -- Switches between hybrid and absolute line numbers
    "jeffkreeftmeijer/vim-numbertoggle",

    -- Quick commands to surround words/lines
    "tpope/vim-surround",

    -- Vim-aware tmux movement
    {
        "aserowy/tmux.nvim",
        config = function()
            return require("tmux").setup({ copy_sync = { enable = false } })
        end
    },
    -- Adds a lot of bindings based off of brackets
    -- TODO: Is this necessary?
    -- {"tpope/vim-unimpaired"},
    -- Multiple cursors
    {
        "mg979/vim-visual-multi",
        init = function()
            vim.cmd([[
            let g:VM_show_warnings = 0
            let g:VM_set_statusline = 0
            let g:VM_silent_exit = 1

            let g:VM_maps = {}

            let g:VM_maps["Find Under"]                  = '<C-n>'
            let g:VM_maps["Find Subword Under"]          = '<C-n>'
            let g:VM_maps["Select All"]                  = '\\A'
            let g:VM_maps["Start Regex Search"]          = '\\/'
            let g:VM_maps["Add Cursor Down"]             = '<C-A-j>'
            let g:VM_maps["Add Cursor Up"]               = '<C-A-k>'
            let g:VM_maps["Add Cursor At Pos"]           = '\\\'

            let g:VM_maps["Visual Regex"]                = '\\/'
            let g:VM_maps["Visual All"]                  = '\\A'
            let g:VM_maps["Visual Add"]                  = '\\a'
            let g:VM_maps["Visual Find"]                 = '\\f'
            let g:VM_maps["Visual Cursors"]              = '\\c'
            ]])
        end
    },

})
