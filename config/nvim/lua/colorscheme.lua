-- local colorscheme = 'onedark'
local colorscheme = 'catppuccin-mocha'

local is_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not is_ok then
    vim.notify('colorscheme ' .. colorscheme .. ' not found!')
    return
end

vim.api.nvim_set_hl(0, 'PmenuSel', { bg = '#1e1e2e', fg = 'NONE' })
vim.api.nvim_set_hl(0, 'Pmenu', { fg = '#cdd6f4', bg = '#181825' })
vim.api.nvim_set_hl(0, 'LspSignatureActiveParameter', { fg = '#cdd6f4', bg = '#181825' })

vim.api.nvim_set_hl(0, 'CmpItemAbbrDeprecated', { fg = '#a6adc8', bg = 'NONE', strikethrough = true })
vim.api.nvim_set_hl(0, 'CmpItemAbbrMatch', { fg = '#89b4fa', bg = 'NONE', bold = true })
vim.api.nvim_set_hl(0, 'CmpItemAbbrMatchFuzzy', { fg = '#89b4fa', bg = 'NONE', bold = true })
vim.api.nvim_set_hl(0, 'CmpItemMenu', { fg = '#b4befe', bg = 'NONE', italic = true })

vim.api.nvim_set_hl(0, 'CmpItemKindField', { fg = '#181825', bg = '#f38ba8' })
vim.api.nvim_set_hl(0, 'CmpItemKindProperty', { fg = '#181825', bg = '#f38ba8' })
vim.api.nvim_set_hl(0, 'CmpItemKindEvent', { fg = '#181825', bg = '#f38ba8' })

vim.api.nvim_set_hl(0, 'CmpItemKindText', { fg = '#181825', bg = '#a6e3a1' })
vim.api.nvim_set_hl(0, 'CmpItemKindEnum', { fg = '#181825', bg = '#a6e3a1' })
vim.api.nvim_set_hl(0, 'CmpItemKindKeyword', { fg = '#181825', bg = '#a6e3a1' })

vim.api.nvim_set_hl(0, 'CmpItemKindConstant', { fg = '#181825', bg = '#f9e2af' })
vim.api.nvim_set_hl(0, 'CmpItemKindConstructor', { fg = '#181825', bg = '#f9e2af' })
vim.api.nvim_set_hl(0, 'CmpItemKindReference', { fg = '#181825', bg = '#f9e2af' })

vim.api.nvim_set_hl(0, 'CmpItemKindFunction', { fg = '#181825', bg = '#cba6f7' })
vim.api.nvim_set_hl(0, 'CmpItemKindStruct', { fg = '#181825', bg = '#cba6f7' })
vim.api.nvim_set_hl(0, 'CmpItemKindClass', { fg = '#181825', bg = '#cba6f7' })
vim.api.nvim_set_hl(0, 'CmpItemKindModule', { fg = '#181825', bg = '#cba6f7' })
vim.api.nvim_set_hl(0, 'CmpItemKindOperator', { fg = '#181825', bg = '#cba6f7' })

vim.api.nvim_set_hl(0, 'CmpItemKindVariable', { fg = '#181825', bg = '#a6adc8' })
vim.api.nvim_set_hl(0, 'CmpItemKindFile', { fg = '#181825', bg = '#a6adc8' })

vim.api.nvim_set_hl(0, 'CmpItemKindUnit', { fg = '#181825', bg = '#fab387' })
vim.api.nvim_set_hl(0, 'CmpItemKindSnippet', { fg = '#181825', bg = '#fab387' })
vim.api.nvim_set_hl(0, 'CmpItemKindFolder', { fg = '#181825', bg = '#fab387' })

vim.api.nvim_set_hl(0, 'CmpItemKindMethod', { fg = '#181825', bg = '#74c7ec' })
vim.api.nvim_set_hl(0, 'CmpItemKindValue', { fg = '#181825', bg = '#74c7ec' })
vim.api.nvim_set_hl(0, 'CmpItemKindEnumMember', { fg = '#181825', bg = '#74c7ec' })

vim.api.nvim_set_hl(0, 'CmpItemKindInterface', { fg = '#181825', bg = '#89dceb' })
vim.api.nvim_set_hl(0, 'CmpItemKindColor', { fg = '#181825', bg = '#89dceb' })
vim.api.nvim_set_hl(0, 'CmpItemKindTypeParameter', { fg = '#181825', bg = '#89dceb' })
