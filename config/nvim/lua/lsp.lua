require('mason').setup({
    ui = {
        icons = {
            package_installed = "✓",
            package_pending = "➜",
            package_uninstalled = "✗"
        }
    }
})

require('mason-lspconfig').setup({
    -- A list of servers to automatically install if they're not already installed
    ensure_installed = { 'clangd', 'bashls', 'pylsp', 'lua_ls', 'rust_analyzer' },
})

-- Set different settings for different languages' LSP
-- LSP list: https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
-- How to use setup({}): https://github.com/neovim/nvim-lspconfig/wiki/Understanding-setup-%7B%7D
--     - the settings table is sent to the LSP
--     - on_attach: a lua callback function to run after LSP attaches to a given buffer
local lspconfig = require('lspconfig')

-- Customized on_attach function
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<space>d', vim.diagnostic.open_float, {
    noremap = true,
    silent = true,
    desc = "Show selected LSP diagnostic message"
})
vim.keymap.set('n', '<space>[', vim.diagnostic.goto_prev, {
    noremap = true,
    silent = true,
    desc = "Go to previous LSP diagnostic message"
})
vim.keymap.set('n', '<space>]', vim.diagnostic.goto_next, {
    noremap = true,
    silent = true,
    desc = "Go to next LSP diagnostic message"
})
vim.keymap.set('n', '<space>D', vim.diagnostic.setloclist, {
    noremap = true,
    silent = true,
    desc = "Show all LSP diagnostic messages"
})

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
    -- Enable completion triggered by <c-x><c-o>
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- See `:help vim.lsp.*` for documentation on any of the below functions
    vim.keymap.set('n', '<space>c', vim.lsp.buf.declaration, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Jump to declaration of selected symbol"
    })
    vim.keymap.set('n', '<space>f', vim.lsp.buf.definition, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Jump to definition of selected symbol"
    })
    vim.keymap.set('n', '<space>i', vim.lsp.buf.hover, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Displays info about symbol"
    })
    vim.keymap.set('n', '<space>t', function() vim.lsp.buf.signature_help() end, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Toggle lsp_signature in normal mode"
    })
    vim.keymap.set('n', '<space>I', vim.lsp.buf.implementation, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "List all implementations for selected symbol in quickfix window."
    })
    vim.keymap.set('n', '<space>f', vim.lsp.buf.signature_help, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Display signature info about selected symbol"
    })
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Add folder to workspace"
    })
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Remove folder from workspace"
    })
    vim.keymap.set('n', '<space>wl', function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "List all workspaces"
    })
    vim.keymap.set('n', '<space>F', vim.lsp.buf.type_definition, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Jump to definition of selected symbol"
    })
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Rename all references to selected symbol"
    })
    vim.keymap.set('n', '<space>a', vim.lsp.buf.code_action, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Selects a code action available at cursor"
    })
    vim.keymap.set('n', '<space>R', vim.lsp.buf.references, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "List all references to selected symbol"
    })
    vim.keymap.set("n", "<space>f", function()
        vim.lsp.buf.format({ async = true })
    end, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "Format buffer using attached LSP"
    })
end

require("mason").setup()
require("mason-lspconfig").setup()
require("mason-lspconfig").setup_handlers {
    -- The first entry (without a key) will be the default handler
    -- and will be called for each installed server that doesn't have
    -- a dedicated handler.
    function(server_name) -- default handler (optional)
        require("lspconfig")[server_name].setup({
            on_attach = on_attach,
        })
    end,

    -- Next, you can provide a dedicated handler for specific servers.
    -- For example, a handler override for the `rust_analyzer`:
    ["lua_ls"] = function()
        lspconfig.lua_ls.setup {
            on_attach = on_attach,
            settings = {
                Lua = {
                    runtime = {
                        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                        version = "LuaJIT",
                    },
                    diagnostics = {
                        -- Get the language server to recognize the `vim` global
                        globals = { "vim" },
                    },
                    workspace = {
                        -- Make the server aware of Neovim runtime files
                        library = vim.env.VIMRUNTIME
                    },
                    -- Do not send telemetry data containing a randomized but unique identifier
                    telemetry = {
                        enable = false,
                    },
                },
            },
        }
    end,
}


vim.api.nvim_create_autocmd("LspAttach", {
    callback = function(args)
        local bufnr = args.buf
        local client = vim.lsp.get_client_by_id(args.data.client_id)
        if vim.tbl_contains({ 'null-ls' }, client.name) then -- blacklist lsp
            return
        end
        require("lsp_signature").on_attach({
            hint_prefix = "󱞩 ",
            handler_opts = {
                border = "none"
            },
        }, bufnr)
    end,
})
