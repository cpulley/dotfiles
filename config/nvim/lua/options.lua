local o = vim.opt

-- Set "," to be leader
vim.g.mapleader = ','

-- Sets how many lines of history VIM has to remember
o.history = 700

-- Line numbers, please
o.number = true

-- Set 7 lines to the cursor - when moving vertically using j/k
o.so = 7

-- Ignore compiled files
-- vim.opt.wildignore=*.o,*~,*.pyc

-- Configure backspace so it acts as it should act
o.backspace = 'eol,start,indent'
o.whichwrap:append {
    ['<'] = true,
    ['>'] = true,
    ['['] = true,
    [']'] = true,
    ['h'] = true,
    ['l'] = true
}

-- Ignore case when searching
o.ignorecase = true

-- When searching try to be smart about cases
o.smartcase = true

-- Show matching brackets when text indicator is over them
o.showmatch = true

-- Truecolor support
o.termguicolors = true

-- UI settings
o.cursorline = true -- highlight cursor line underneath the cursor horizontally
o.splitbelow = true -- open new vertical split bottom
o.splitright = true -- open new horizontal splits right

-- Tab settings
o.tabstop = 4      -- number of visual spaces per TAB
o.softtabstop = 4  -- number of spacesin tab when editing
o.shiftwidth = 4   -- insert 4 spaces on a tab
o.expandtab = true -- tabs are spaces, mainly because of python
