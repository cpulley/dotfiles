# This file is part of ranger, the console file manager.
# License: GNU GPL version 3, see the file "AUTHORS" for details.

from __future__ import (absolute_import, division, print_function)

from ranger.colorschemes.default import Default
from ranger.gui.color import black, blue, cyan, green, magenta, red, white, yellow, default, normal, bold, blink, reverse, underline, invisible, dim


class Scheme(Default):
    def use(self, context):
        fg, bg, attr = Default.use(self, context)

        if context.in_titlebar:
            if context.tab:
                if context.good:
                    bg = default
                    fg = green
                    attr = bold
                else:
                    bg = default
                    fg = white
                    attr = normal


        return fg, bg, attr
