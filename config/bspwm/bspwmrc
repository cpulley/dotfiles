#! /bin/sh

############################################
#     _         _                          #
#    / \  _   _| |_ ___  _ __ _   _ _ __   #
#   / _ \| | | | __/ _ \| '__| | | | '_ \  #
#  / ___ \ |_| | || (_) | |  | |_| | | | | #
# /_/   \_\__,_|\__\___/|_|   \__,_|_| |_| #
#                                          #
############################################

sxhkd &

# Fix Cursor
xsetroot -cursor_name left_ptr &

# Set capslock button to escape
# setxkbmap -option caps:escape &

# Enable wallpaper
sh ~/.fehbg &

# Get audio running early
pulseaudio --start

# Enable notifications via Dunst
deadd-notification-center &

# Start compositor
picom &

# Start gnome settings daemons (for bspwm-gnome)
/usr/lib/gnome-settings-daemon/gsd-xsettings &
gnome-power-manager &

# Launch Polybar
sh ~/.scripts/bspwm/polybar_start &

# Initialize locker
xautolock -time 20 -locker "i3lock -e -c eeeeee -S 1 --indicator -k --insidevercolor=718c0099 --insidewrongcolor=d7005f99 --insidecolor=edededff --ringvercolor=718c00ff --ringwrongcolor=d7005fff --ringcolor=4d4d4c66 --line-uses-ring --keyhlcolor=4271aeff --bshlcolor=d7005fff --separator=969694ff --radius=110 --time-font='M+ 1mn' --timecolor=4d4d4cff --date-font='M+ 1mn' --datecolor=4d4d4cff --layout-font='M+ 1mn' --verif-font='M+ 1mn' --wrong-font='M+ 1mn' --greeter-font='M+ 1mn' --greetercolor=4d4d4cff --veriftext='Logging in…' --wrongtext='Try again.' --noinputtext='Prompt empty.' --pass-media-keys --pass-screen-keys --pass-power-keys"

##########################################
#  ____       _   _   _                  #
# / ___|  ___| |_| |_(_)_ __   __ _ ___  #
# \___ \ / _ \ __| __| | '_ \ / _` / __| #
#  ___) |  __/ |_| |_| | | | | (_| \__ \ #
# |____/ \___|\__|\__|_|_| |_|\__, |___/ #
#                             |___/      #
##########################################

bspc config border_width        2
bspc config window_gap         6

bspc config split_ratio         0.52

bspc config focus_follows_pointer false
bspc config pointer_follows_focus true
bspc config pointer_follows_monitor true

bspc config active_border_color \#282c34
bspc config focused_border_color \#abb2bf
bspc config presel_feedback_color \#999999

bspc monitor DVI-D-0 -d DVI
bspc monitor HDMI-0 -d HDMI
bspc monitor VGA-0 -d VGA
bspc monitor LVDS1 -d LVDS1

bspc config bottom_padding 0
bspc config top_padding 26


#############################
#  ____        _            #
# |  _ \ _   _| | ___  ___  #
# | |_) | | | | |/ _ \/ __| #
# |  _ <| |_| | |  __/\__ \ #
# |_| \_\\__,_|_|\___||___/ #
#                           #
#############################

# Steam's normal desktop windows have both class and instance Steam
# while Steam's big picture mode windows have class/instance steam.
# To get normal steam windows tiled and functional big picture mode
# and steam keyboard windows, we have to default to focus=off/floating.
# Big picture mode defaults to max resolution, so it appears to be
# fullscreen, but fullscreen and floating games /should/ draw above it.
# Alongside that, if steam keyboard has focus, it tries to type into itself.
bspc rule -a steam:steam           state=floating       floating=on       focus=off         border=off layer=normal

bspc rule -a Com.github.geigi.cozy desktop="Audiobooks" monitor="HDMI-0"
bspc rule -a strawberry            desktop="Music"      monitor="HDMI-0"
bspc rule -a com.github.needleandthread.vocal desktop="Podcasts" monitor="HDMI-0"
bspc rule -a Spotify               desktop="Spotify"    monitor="HDMI-0"
bspc rule -a Steam                 desktop="Steam"      monitor="DVI-D-0" state=tiled
bspc rule -a battle.net            desktop="Battle.net" monitor="VGA-0"
bspc rule -a Ripcord               desktop="Chat"       monitor="VGA-0"
bspc rule -a discord               desktop="Chat"       monitor="VGA-0"
