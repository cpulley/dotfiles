config.load_autoconfig()

#############################################
#  _  __          _     _           _       #
#  | |/ /___ _   _| |__ (_)_ __   __| |___  #
#  | ' // _ \ | | | '_ \| | '_ \ / _` / __| #
#  | . \  __/ |_| | |_) | | | | | (_| \__ \ #
#  |_|\_\___|\__, |_.__/|_|_| |_|\__,_|___/ #
#           |___/                           #
#############################################

config.bind(',b', 'set-cmd-text -s :buffer')
config.bind('sbd', 'spawn /home/cpulley/.scripts/base64_decode')
config.bind('sfm', 'hint links spawn smplayer {hint-url}')
config.bind('sm', 'spawn smplayer {url}')
config.bind('sfyd', 'hint links spawn youtube-dl --output "/home/cpulley/Downloads/%(title)s.%(ext)s" {hint-url}')
config.bind('sfya', 'hint links spawn youtube-dl -x --audio-format mp3 --output "/home/cpulley/Downloads/%(title)s.%(ext)s" {hint-url}')
config.bind('syd', 'spawn youtube-dl --output "/home/cpulley/Downloads/%(title)s.%(ext)s" {url}')
config.bind('sya', 'spawn youtube-dl -x --audio-format mp3 --output "/home/cpulley/Downloads/%(title)s.%(ext)s" {url}')
config.bind('sy', 'hint links spawn youtube-dl')
config.bind('yf', 'hint links yank')
config.bind('m', 'set-cmd-text -s :bookmark-load')
config.bind('M', 'set-cmd-text -s :bookmark-load -t')
config.bind('wm', 'set-cmd-text -s :bookmark-load -w')
config.bind('<ctrl-i>', 'enter-mode passthrough')
config.bind('<ctrl-i>', 'leave-mode', mode='passthrough')
config.bind('<ctrl-b>', 'quickmark-save')
config.bind('<ctrl-m>', 'bookmark-add')
config.bind('<alt-b>', 'set-cmd-text -s :quickmark-del')
config.bind('<alt-m>', 'set-cmd-text -s :bookmark-del')

config.unbind('sf', mode='normal')
config.unbind('sk', mode='normal')
config.unbind('sl', mode='normal')
config.unbind('ss', mode='normal')
config.unbind('<ctrl-v>', mode='normal')
config.unbind ('<shift-escape>', mode='passthrough')

#################################
#   ____      _                 #
#  / ___|___ | | ___  _ __ ___  #
# | |   / _ \| |/ _ \| '__/ __| #
# | |__| (_) | | (_) | |  \__ \ #
#  \____\___/|_|\___/|_|  |___/ #
#                               #
#################################

c.colors.completion.category.bg = '#282c34'
c.colors.completion.category.border.bottom = '#abb2bf'
c.colors.completion.category.border.top = '#abb2bf'
c.colors.completion.category.fg = '#abb2bf'
c.colors.completion.even.bg = '#282c34'
c.colors.completion.fg = '#abb2bf'
c.colors.completion.item.selected.bg = '#4b5263'
c.colors.completion.item.selected.border.bottom = '#abb2bf'
c.colors.completion.item.selected.border.top = '#abb2bf'
c.colors.completion.item.selected.fg = '#abb2bf'
c.colors.completion.match.fg = '#56b6c2'
c.colors.completion.odd.bg = '#282c34'
c.colors.completion.scrollbar.bg = '#282c34'
c.colors.completion.scrollbar.fg = '#abb2bf'
c.colors.downloads.bar.bg = '#282c34'
c.colors.downloads.error.bg = '#be5046'
c.colors.downloads.error.fg = '#282c34'
c.colors.downloads.start.bg = '#222222'
c.colors.downloads.start.fg = '#abb2bf'
c.colors.downloads.stop.bg = '#61afef'
c.colors.downloads.stop.fg = '#282c34'
c.colors.downloads.system.bg = 'hsl'
c.colors.downloads.system.fg = 'hsl'
c.colors.hints.bg = '#282c34'
c.colors.hints.fg = '#abb2bf'
c.colors.hints.match.fg = '#56b6c2'
c.colors.keyhint.bg = 'rgba(0, 0, 0, 80%)'
c.colors.keyhint.fg = '#FFFFCC'
c.colors.keyhint.suffix.fg = '#FFFF00'
c.colors.messages.error.bg = '#be5046'
c.colors.messages.error.border = '#bb0000'
c.colors.messages.error.fg = 'white'
c.colors.messages.info.bg = '#383c44'
c.colors.messages.info.border = '#383c44'
c.colors.messages.info.fg = '#abb2bf'
c.colors.messages.warning.bg = '#d19a66'
c.colors.messages.warning.border = '#d47300'
c.colors.messages.warning.fg = '#282c34'
c.colors.prompts.bg = '#444444'
c.colors.prompts.border = '1px solid gray'
c.colors.prompts.fg = 'white'
c.colors.prompts.selected.bg = 'grey'
c.colors.statusbar.caret.bg = 'purple'
c.colors.statusbar.caret.fg = 'white'
c.colors.statusbar.caret.selection.bg = '#a12dff'
c.colors.statusbar.caret.selection.fg = 'white'
c.colors.statusbar.command.bg = '#282c34'
c.colors.statusbar.command.fg = '#abb2bf'
c.colors.statusbar.command.private.bg = '#cdcdcd'
c.colors.statusbar.command.private.fg = '#abb2bf'
c.colors.statusbar.insert.bg = '#98c379'
c.colors.statusbar.insert.fg = '#282c34'
c.colors.statusbar.normal.bg = '#282c34'
c.colors.statusbar.normal.fg = '#abb2bf'
c.colors.statusbar.passthrough.bg = 'darkblue'
c.colors.statusbar.passthrough.fg = 'white'
c.colors.statusbar.private.bg = '#cdcdcd'
c.colors.statusbar.private.fg = '#abb2bf'
c.colors.statusbar.progress.bg = 'white'
c.colors.statusbar.url.error.fg = '#aa884c'
c.colors.statusbar.url.fg = '#abb2bf'
c.colors.statusbar.url.hover.fg = '#4c4caa'
c.colors.statusbar.url.success.http.fg = '#abb2bf'
c.colors.statusbar.url.success.https.fg = '#61afef'
c.colors.statusbar.url.warn.fg = '#aaaa4c'
c.colors.tabs.bar.bg = '#282c34'
c.colors.tabs.even.bg = '#282c34'
c.colors.tabs.even.fg = '#bbc2cf'
c.colors.tabs.indicator.error = '#be5046'
c.colors.tabs.indicator.start = '#282c34'
c.colors.tabs.indicator.stop = '#282c34'
c.colors.tabs.indicator.system = 'rgb'
c.colors.tabs.odd.bg = '#282c34'
c.colors.tabs.odd.fg = '#bbc2cf'
c.colors.tabs.selected.even.bg = '#4b5263'
c.colors.tabs.selected.even.fg = '#cbd2df'
c.colors.tabs.selected.odd.bg = '#4b5263'
c.colors.tabs.selected.odd.fg = '#cbd2df'

#######################################
#   ____                           _  #
#  / ___| ___ _ __   ___ _ __ __ _| | #
# | |  _ / _ \ '_ \ / _ \ '__/ _` | | #
# | |_| |  __/ | | |  __/ | | (_| | | #
#  \____|\___|_| |_|\___|_|  \__,_|_| #
#                                     #
#######################################

c.completion.height = '30%'
c.completion.scrollbar.width = 12
c.completion.show = 'always'
c.completion.shrink = True
c.content.default_encoding = 'iso-8859-1'
c.content.geolocation = False
c.content.headers.accept_language = 'en-US,en'
c.content.plugins = False
c.content.print_element_backgrounds = True
c.colors.contextmenu.menu.bg = '#282c34'
c.colors.contextmenu.menu.fg = '#abb2bf'
c.colors.contextmenu.selected.bg = '#abb2bf'
c.colors.contextmenu.selected.fg = '#282c34'
c.downloads.remove_finished = 60000
c.fonts.completion.category = '9pt "Noto Sans Semicondensed"'
c.fonts.completion.entry = '9pt "Noto Sans Semicondensed"'
c.fonts.debug_console = '9pt "M+1 mn"'
c.fonts.downloads = '9pt "Noto Sans Semicondensed"'
c.fonts.hints = '11pt "Noto Sans Semicondensed"'
c.fonts.keyhint = '11pt "Noto Sans Semicondensed"'
c.fonts.messages.error = '9pt "Noto Sans Semicondensed"'
c.fonts.messages.info = '9pt "Noto Sans Semicondensed"'
c.fonts.messages.warning = '9pt "Noto Sans Semicondensed"'
c.fonts.contextmenu = '9pt "Noto Sans Semicondensed"'
c.fonts.default_family = '"Noto Sans Semicondensed", "xos4 Terminus", Terminus, Monospace, "DejaVu Sans Mono", Monaco,   "Bitstream Vera Sans Mono", "Andale Mono", "Courier New", Courier, "Liberation   Mono", monospace, Fixed, Consolas, Terminal'
c.fonts.prompts = '9pt "Noto Sans Semicondensed"'
c.fonts.statusbar = '9pt "Noto Sans Semicondensed"'
c.fonts.tabs.selected = '9pt "Noto Sans Semicondensed"'
c.fonts.tabs.unselected = '9pt "Noto Sans Semicondensed"'
c.fonts.web.family.cursive = 'Noto Sans Semicondensed'
c.fonts.web.family.fantasy = 'Noto Sans Semicondensed'
c.fonts.web.family.fixed = 'M+1 mn'
c.fonts.web.family.sans_serif = 'Noto Sans Semicondensed'
c.fonts.web.family.serif = 'Noto Sans Semicondensed'
c.fonts.web.family.standard = 'Noto Sans Semicondensed'
c.fonts.web.size.default = 16
c.hints.auto_follow_timeout = 250
c.hints.border = '1px solid #abb2bf'
c.hints.hide_unmatched_rapid_hints = True
c.hints.min_chars = 1
c.hints.mode = 'number'
c.hints.next_regexes = ['next', 'more', 'newer', '[>→≫]', '(>>|»)', 'continue']
c.hints.prev_regexes = ['prev(ious)?', 'back', 'older', '[<←≪]', '(<<|«)']
c.hints.scatter = True
c.input.forward_unbound_keys = 'auto'
c.input.insert_mode.auto_leave = True
c.input.insert_mode.auto_load = False
c.input.insert_mode.leave_on_load  = False
c.input.insert_mode.plugins = False
c.input.links_included_in_focus_chain = True
c.input.partial_timeout = 5000
c.input.mouse.rocker_gestures = False
c.input.spatial_navigation = False
c.keyhint.blacklist = ['null']
c.keyhint.delay = 500
c.messages.timeout = 2000
c.prompt.filebrowser = True
c.prompt.radius = 8
c.qt.args = ['null']
c.qt.force_software_rendering = 'software-opengl'
c.scrolling.bar = 'when-searching'
c.scrolling.smooth = True
c.spellcheck.languages = ['en-US', 'en-GB']
c.statusbar.show = 'always'
c.statusbar.padding =  { 'bottom': 1, 'left': 0, 'right': 0, 'top': 1 }
c.statusbar.position = 'bottom'
c.tabs.background = True
c.tabs.close_mouse_button = 'middle'
c.tabs.close_mouse_button_on_bar = 'new-tab'
c.tabs.favicons.scale = 1.1
c.tabs.favicons.show = 'always'
c.tabs.indicator.padding = { 'bottom': 0, 'left': 1, 'right': 1, 'top': 0 }
c.tabs.indicator.width = 0
c.tabs.last_close = 'close'
c.tabs.mousewheel_switching = True
c.tabs.new_position.related = 'next'
c.tabs.new_position.unrelated = 'last'
c.tabs.padding = { 'bottom': 6, 'left': 4, 'right': 2, 'top': 6 }
c.tabs.position = 'left'
c.tabs.select_on_remove = 'next'
c.tabs.show = 'always'
c.tabs.show_switching_delay = 800
c.tabs.tabs_are_windows = False
c.tabs.title.alignment = 'left'
c.tabs.title.format = ' {current_title}'
c.tabs.title.format_pinned = '{index}'
c.tabs.width = 190
c.tabs.wrap = True
c.url.auto_search = 'naive'
c.url.default_page = 'https://start.duckduckgo.com/'
c.url.incdec_segments = ['path',  'query']
c.url.searchengines = {'DEFAULT': 'https://duckduckgo.com/?q={}'}
c.url.start_pages = 'https://start.duckduckgo.com'
c.url.yank_ignored_parameters = ['ref', 'utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content']
c.window.hide_decoration = False
c.window.title_format = '{perc}{current_title}{title_sep}qutebrowser'
c.zoom.default = '85%'
c.zoom.levels = ['25%', '33%', '50%', '67%', '75%', '90%', '100%', '110%', '125%', '150%', '175%', '200%', '250%', '300%', '400%', '500%']
