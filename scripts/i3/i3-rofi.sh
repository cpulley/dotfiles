#!/usr/bin/env bash

# Print a list of workspaces for rofi to use as selectors
workspaceList () {
	activeWorkspaces=$(i3-msg -t get_workspaces | tr , '\n' | grep name | cut -d \" -f 4 | sort)
	defaultWorkspaces="$(cat ~/.i3-ws-history | sed -E 's|[0-9]*:(.*$)|\1|')"

	# Loop to remove actively used workspaces from the default list
	# Really not *necessary* but avoids aestheticly displeasing duplicates.
	# We use a "while read" to avoid issues with workspaces with spaces in their names.
	inactiveWorkspaces=$(while read workspace; do
			for exists in cmd $activeWorkspaces; do
				if [[ "$workspace" == "$exists" ]]; then
					workspace=""
					break
				fi
			done

			if [ ! -z "$workspace" ]; then
				echo $workspace
			fi
		done <<< $defaultWorkspaces
	)

	echo -en "                      Active,$activeWorkspaces,,                    Inactive,$inactiveWorkspaces" | tr , '\n' | sed -E 's|([0-9]*):(.*)|\2\x00Info\x1f\1|mg'
}

# Add a selected option to a history file for future use
historyUpdate () {
	if [[ ! -f ~/.i3-ws-history ]]; then
		touch ~/.i3-ws-history
	fi

	if [ $# -gt 0 ]; then
		# Separate arguments into a priority number and a name
		# This is useful so that if we create a workspace with a different priority
		# we don't accidentally create a duplicate listing.
		entry="$(echo $* | cut -d : -f 2-)"
		number="$(echo $* | cut -s -d : -f 1)"

		# I need to create a bit of code here to error out if entry has bad text in it.
		# This isn't strictly necessary (i3 is *very* lenient) but it's probably a
		# good idea anyways.

		# If we didn't give an explicit priority, default to last
		if [ -z $number ]; then
			if [ -z $ROFI_INFO ]; then
				number="$(expr $(wc -l ~/.i3-ws-history | cut -d " " -f 1) + 1)"
			else
				number=$ROFI_INFO
			fi
		fi

		# Search history for our entry name, and if it exists, assign that full line
		# to oldEntry for sed
		oldEntry="$(grep -E -m1 "^.*:$entry$" ~/.i3-ws-history)"

		# If we found an entry with grep, update the priority and add 1 to its ranking
		# Else, make a new entry.
		if [[ -n $oldEntry ]]; then
			newEntry="$(expr $(echo $oldEntry | cut -d : -f 1) + 1):$number:$entry"
			sed -i "s|^$oldEntry$|$newEntry|" ~/.i3-ws-history
		else
			echo 0:$number:$entry >> ~/.i3-ws-history
		fi

		# Make sure each workspace has a unique number
		# First, we reorder each line to be (WS Number)(Name)(Popularity)
		# We sort those, so that we preserve the general order of things
		# Then, we replace each number by counting through each line
		# Finally, we return to (Popularity)(WS Number)(Name) and sort again
		let count=1
		while IFS= read -r line; do
			echo $line | sed -E "s|([0-9]*):(.*):([0-9]*)|\3:$count:\2|"
			let count=$count+1
		done <<< "$(cat .i3-ws-history | sed -E 's|([0-9]*):([0-9]*):(.*)|\2:\3:\1|' | sort -g)" | sort -gr -o ~/.i3-ws-history

		# Final read, run through all active workspaces and update their names
		while IFS= read -r workspace; do
			newWorkspace=$(grep -e "$(echo $workspace | cut -d : -f 2)$" ~/.i3-ws-history | cut -d : -f 2-)
			i3-msg rename workspace \"$workspace\" to \"$newWorkspace\" > /dev/null
		done <<< $(i3-msg -t get_workspaces | tr , '\n' | grep name | cut -d \" -f 4)

	fi
}

if [[ -z $ROFI_DATA ]]; then
	# Save $1 in $ROFI_DATA
	echo -en "\0data\x1f$1\n"
	case ${1} in
		--move)   echo -en "\0prompt\x1f \n";;
		--switch) echo -en "\0prompt\x1f \n";;
		--rename) echo -en "\0prompt\x1f \n \n";;
		*) echo "ERROR" && exit 1;;
	esac
	workspaceList
else
	shift
	historyUpdate $*
	if [ $ROFI_DATA == "--move" ]; then
		i3-msg move container to workspace \"$number:$entry\", workspace \"$number:$entry\" > /dev/null
	elif [ $ROFI_DATA == "--switch" ]; then
		i3-msg workspace \"$number:$entry\" > /dev/null
	elif [ $ROFI_DATA == "--rename" ]; then
		i3-msg rename workspace to \"$number:$entry\" > /dev/null
		exit 0
	fi
fi

