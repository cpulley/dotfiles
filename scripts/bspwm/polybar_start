#!/bin/bash
CHASSIS="$(< /sys/class/dmi/id/chassis_type)"
HOSTNAME="$(hostname)"

# Kill any existing polybar instances
killall polybar

# If hostname equals my personal desktop, use personalized settings
if [[ $HOSTNAME == narayan ]]; then
	MONITOR=DVI-D-0 polybar --reload bspwmbar-primary &
	MONITOR=HDMI-0 polybar --reload bspwmbar &
	MONITOR=VGA-0 polybar --reload bspwmbar-mini &

# If $CHASSIS appears to be a laptop, enable battery display
# (See https://technet.microsoft.com/en-us/library/ee156537.aspx )
#  8 - Portable
#  9 - Laptop  
# 10 - Notebook  
# 11 - Hand Held  
# 12 - Docking Station  
# 14 - Sub Notebook
elif [[ $CHASSIS -eq 8	]] || \
     [[ $CHASSIS -eq 9  ]] || \
     [[ $CHASSIS -eq 10 ]] || \
     [[ $CHASSIS -eq 11 ]] || \
     [[ $CHASSIS -eq 12 ]] || \
     [[ $CHASSIS -eq 14 ]]     
then
	if type "xrandr"; then
		for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
			MONITOR=$m polybar --reload bspwmbar-laptop &
		done
		for m in $(xrandr --query | grep " connected" | grep -v " primary" | cut -d" " -f1); do
			MONITOR=$m polybar --reload bspwmbar &
		done

	fi

# If this is a different desktop, run default settings
# on all available monitors
elif type "xrandr"; then
	for m in $(xrandr --query | grep " connected" | grep " primary" | cut -d" " -f1); do
		MONITOR=$m polybar --reload bspwmbar-primary &
	done
	for m in $(xrandr --query | grep " connected" | grep -v " primary" | cut -d" " -f1); do
		MONITOR=$m polybar --reload bspwmbar &
	done

# If xrandr doesn't work, just launch on default monitor
else
	polybar --reload bspwmbar &
fi

exit 0
